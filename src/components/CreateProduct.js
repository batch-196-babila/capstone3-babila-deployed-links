import { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

export default function CreateProduct() {
    const [show, setShow] = useState(false);
    const handleCloseCreate = () => setShow(false);
    const handleShowCreate = () => setShow(true);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [isActiveBtn, setIsActiveBtn] = useState(false);

    function createNewProduct(e) {
        fetch("https://shrouded-coast-15740.herokuapp.com/products/create", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then((data) => {
            // console.log(data);
            setName('');
            setDescription('');
            setPrice('');
            handleCloseCreate();  
            window.location.reload(true);
        })

    }

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '' && price > 0) {
            setIsActiveBtn(true);
        } 
        else {
            setIsActiveBtn(false);
        }
    }, [name, description, price]);

    return (
        <>
        <Button className="mt-5" variant="outline-dark" onClick={handleShowCreate}>
            CREATE NEW PRODUCT
        </Button>

        <Modal show={show} onHide={handleCloseCreate}>
            <Modal.Header closeButton>
                <Modal.Title>Add New Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={e => createNewProduct(e)}>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter product name"
                            value={name}
                            autoFocus
                            onChange={e => setName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group
                        className="mb-3"
                        controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows={3}
                            placeholder="Enter description"
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter price"
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                            autoFocus
                        />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseCreate}>
                    Close
                </Button>
            { (isActiveBtn === true) ?
                <Button variant="primary" type="submit" id="submitBtn" onClick={(e) => createNewProduct(e)}>
                         Save New Product
                 </Button>
                :
                <Button variant="primary" disabled>
                    Save New Product
                </Button>
            }
   
            </Modal.Footer>
        </Modal>
        </>
    );
}