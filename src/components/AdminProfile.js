import { useEffect, useState } from 'react';
import { Container, Row, Col, ListGroup, Image} from 'react-bootstrap';
import '../style/styles.css';

export default function AdminProfile(){
    const [firstName , setFirstName] = useState('');
    const [lastName , setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo , setMobileNo] = useState('');

    useEffect(() => {
        fetch("https://shrouded-coast-15740.herokuapp.com/users/getUserDetails", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {
            // console.log(data._id);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setMobileNo(data.mobileNo);
        })
    }, []);


    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{span:6, offset:3}}>
                    
                    <ListGroup className="profile-list-group" variant="flush">
                        <Image className="profile-logo" src={require('../images/profile.png')} />
                        <h1 className="title-header">PROFILE DETAILS</h1>
                        <ListGroup.Item></ListGroup.Item>
                        <ListGroup.Item>{firstName} {lastName}</ListGroup.Item>
                        <ListGroup.Item>{email}</ListGroup.Item>
                        <ListGroup.Item>{mobileNo}</ListGroup.Item>
                        <ListGroup.Item></ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    );
}

