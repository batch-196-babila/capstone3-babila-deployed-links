import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';


export default function ProductList({ productProp }) {

    const { _id, name, description, price, isActive } = productProp;

    // UPDATE MODAL
    const [showUpdate, setShowUpdate] = useState(false);
    const handleCloseUpdate = () => setShowUpdate(false);
    const handleShowUpdate = () => setShowUpdate(true);

    // DELETE MODALS
    // CONFIRM USER BEFORE DELETING
    const [showDelete, setShowDelete] = useState(false);
    const handleCloseDelete = () => setShowDelete(false);
    const handleShowDelete = () => setShowDelete(true);
   
    // ARCHIVE & UNARCHIVE
    const [itemIsActive, setItemIsActive] = useState(isActive);

    function updateProduct(productId) {

        fetch(`https://shrouded-coast-15740.herokuapp.com/products/update/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: productName,
                description: productDescription,
                price: productPrice,
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data._id);
            setShowUpdate(false);
            pageReload();
        })
    }

    function deleteProduct(productId) {
        fetch(`https://shrouded-coast-15740.herokuapp.com/products/delete/${productId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            setShowDelete(false);
            pageReload();
        })
    }

    function unarchiveProduct(_id){
		fetch(`https://shrouded-coast-15740.herokuapp.com/products/unarchive/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
            // console.log(data)
            setItemIsActive(true);
            pageReload();
		})
	}

	function archiveProduct(_id){
		fetch(`https://shrouded-coast-15740.herokuapp.com/products/archive/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
            setItemIsActive(false);
            pageReload();
		})
        
	}

    function pageReload() {
        window.location.reload(true);
    }

    // UPDATE MODALS
    // POPULATE CURRENT VALUE

    const [productName, setProductName] = useState(name);
    const [productDescription, setProductDescription] = useState(description);
    const [productPrice, setProductPrice] = useState(price);

    return (
        <>
            <tr variant="primary">
                <td>{ name }</td>
                <td>{ description }</td>
                <td>{ price }</td>
                <td>{ isActive ? "Available" : "Unavailable" }</td>
                <td style={{width: 30 * 10}}>
                    <Button variant="success" onClick={handleShowUpdate}>Update</Button>&nbsp;
                    <Button variant="danger" onClick={handleShowDelete}>Delete</Button>&nbsp;
                    {	itemIsActive ?
                        <Button variant="warning" onClick={() => archiveProduct(_id)}>Deactivate</Button>
                        :			         	
                        <Button variant="outline-warning" style={{width: 10 * 10}} onClick={() => unarchiveProduct(_id)}>Activate</Button>		
                    }	
                </td>
            </tr>

            {/* UPDATE MODAL */}
            <Modal show={showUpdate} onHide={handleCloseUpdate} >
                <Modal.Header closeButton>
                    <Modal.Title>Edit Product's Info</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder=" "
                                value={productName}
                                onChange={e => setProductName(e.target.value)}
                                autoFocus
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder=""
                                value={productDescription}
                                onChange={e => setProductDescription(e.target.value)}
                                autoFocus
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder=""
                                value={productPrice}
                                onChange={e => setProductPrice(e.target.value)}
                                autoFocus
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseUpdate}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={() => updateProduct(_id)}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>

            {/* DELETE MODAL */}
            <Modal show={showDelete} onHide={handleCloseDelete} backdrop="static" keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Warning!</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5>Are you sure you want to permanently delete this item?</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseDelete}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={() => deleteProduct(_id)}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}