import { useContext, useEffect, useState } from 'react';
import { Card, Button, Modal } from "react-bootstrap";
import { Link } from 'react-router-dom';
import UserContext from "../UserContext";
import '../style/styles.css';

export default function ShopList({productProp, index}) { 
    // CHECK IF USER IS LOGGED IN AT THE SHOP PAGE
    const {user} = useContext(UserContext);

    // PRODUCT DISPLAY
    const {name, description, price, _id} = productProp;    

    const [productName, setProductName] = useState('');
    const [productDescription , setProductDescription] = useState('');
    const [productPrice , setProductPrice] = useState(0);
    
    // CONFIRM PURCHASE
    const [showConfirm, setShowConfirm] = useState(false);
    const handleCloseConfirm = () => setShowConfirm(false);
    const handleShowConfirm = () => setShowConfirm(true);

    // PURCHASE SUCCESS MESSAGE
    const [showInfo, setShowInfo] = useState(false);
    const handleCloseInfo = () => setShowInfo(false);
    const handleShowInfo = () => setShowInfo(true);

    const placeOrder = (productId) => {
        fetch('https://shrouded-coast-15740.herokuapp.com/users/checkoutSingleOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: 1,
                totalAmount: price
            })
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data)
            handleCloseConfirm();
            handleShowInfo();
        })
    }

    useEffect(() =>{
        fetch(`https://shrouded-coast-15740.herokuapp.com/products/getSingleProduct/${_id}`)
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setProductName(data.name);
            setProductDescription(data.description);
            setProductPrice(data.price);
        })
    },[_id]);

    return (
        <>
        <Card className='shop-row box boxes' bg='light' style={{width: "24rem"}} key={index}>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>{description} <br/> PHP {price}</Card.Text>
                { user.id !== null ?
                    <Button variant="outline-dark" onClick={handleShowConfirm}>Purchase</Button>
                    :
                    <Button variant="outline-dark" as={Link} to={'/login'}>Login to Purchase</Button>
                    }
            </Card.Body>
        </Card>

        {/* CONFIRM PURCHASE MODAL */}
        <Modal show={showConfirm} onHide={handleCloseConfirm} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Confirm your purchase</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p><strong>ORDER DETAILS</strong></p>
                <p>{productName} - {productDescription} <br/>Quantity: 1<br/>
                Total Amount: PHP {productPrice} <br/></p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseConfirm}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={() => placeOrder(_id)}>
                    Confirm
                </Button>
            </Modal.Footer>
        </Modal>

        <Modal show={showInfo} onHide={handleCloseConfirm} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Order Created</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h5>Your purchase was successful.<br /> Please check your profile for the latest purchase.</h5>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleCloseInfo}>
                    Okay
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )
};