import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from'./pages/Logout';
import Home from './pages/Home';
import Shop from './pages/Shop';
import Profile from './pages/Profile';
import Dashboard from './pages/Dashboard';
import AdminViewAllOrders from './pages/AdminViewAllOrders';
import Error from './pages/Error';
import AppNavbar from './components/AppNavbar';
import AppFooter from './components/AppFooter';
import './App.css';

function App() {
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
    };

    useEffect(() => {
        fetch("https://shrouded-coast-15740.herokuapp.com/users/getUserDetails", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {
            //console.log(data);

            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                    // PASS OTHER DETAILS
                    firstName: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    mobileNo: data.mobileNo
                })
            }
            else {
                // set back the initial state of user
                setUser({
                    id: null,
                    isAdmin: null,
                    firstName: null,
                    lastName: null,
                    email: null,
                    mobileNo: null
                })
            }
        })
    }, []);

    return (
        <>
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <AppNavbar />
                <Container>
                    <Routes>
                        <Route exact path="/" element={<Home/>} />
                        <Route exact path="/login" element={<Login/>} />
                        <Route exact path="/profile" element={<Profile/>} />
                        <Route exact path="/shop" element={<Shop/>} />
                        <Route exact path="/admin/dashboard" element={<Dashboard/>} />
                        <Route exact path="/admin/viewAllOrders" element={<AdminViewAllOrders/>} />
                        <Route exact path="/register" element={<Register/>} />
                        <Route exact path="/logout" element={<Logout/>} />
                        <Route exact path="*" element={<Error/>} />
                    </Routes>
                </Container>
                <AppFooter />
            </Router> 
        </UserProvider>
        </>
    )
}

export default App;