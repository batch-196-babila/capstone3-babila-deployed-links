import { useEffect, useState, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
    const { user } = useContext(UserContext);
    const redirect = useNavigate();

    const [firstName , setFirstName] = useState('');
    const [lastName , setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [mobileNo , setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        e.preventDefault();
        // CHECK EMAIL IF EXISTING
        fetch('https://shrouded-coast-15740.herokuapp.com/users/checkEmailExists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(result => result.json())
        .then(data => {
            // console.log(data);
            if (data) {
                Swal.fire({
                    title: "Failed",
                    icon: "info",
                    text: "Email already exists."
                })
            }
            else {
                // REGISTER USER IF EMAIL NOT EXISTING
                fetch('https://shrouded-coast-15740.herokuapp.com/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,                       
                        mobileNo: mobileNo,
                        email: email,
                        password: password
                    })
                })
                .then(response => response.json())
                .then(data => {
                    if (data.email) {
                        Swal.fire({
                            title: "Registration successful",
                            icon: "success",
                            text: "Thank you for registering."
                        })
                        redirect('/login');
                    }
                    else {
                        Swal.fire({
                            title: "Registration failed",
                            icon: "error",
                            text: "Something went wrong, try again."
                        })
                    }
                })
            }
        })
    }

    useEffect(() => {
        if (email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11 && password === verifyPassword) {
            setIsActive(true);
        } 
        else {
            setIsActive(false);
        }
    }, [email, firstName, lastName, mobileNo, password, verifyPassword]); 

    return (
        (user.id !== null) ?
            <Navigate to="/" />
        :
        <>
        <Form className="login-form" onSubmit={e => registerUser(e)}>
            <h4 className="title-header">R E G I S T E R</h4>
            <p className="subtitle-header">Please fill in the information below:</p>
            <Form.Group  className="login-input" controlId="firstName">
                <Form.Control
                    className="input-height" 
                    type="text" 
                    placeholder="First name" 
                    required 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                />
            </Form.Group>
            <Form.Group  className="login-input" controlId="lastName">
                <Form.Control 
                    className="input-height" 
                    type="text" 
                    placeholder="Last name" 
                    required 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                />
            </Form.Group>
            <Form.Group  className="login-input" controlId="mobileNo">
                <Form.Control 
                    className="input-height" 
                    type="text" 
                    placeholder="Mobile number (11 digits)" 
                    required 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                />
            </Form.Group>
            <Form.Group className="login-input" controlId="userEmail">
                <Form.Control 
                    className="input-height" 
                    type="email" 
                    placeholder="Email" 
                    required 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group  className="login-input" controlId="password">
                <Form.Control 
                    className="input-height" 
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group>

            <Form.Group  className="login-input" controlId="password">
                <Form.Control 
                    className="input-height" 
                    type="password" 
                    placeholder="Verify password" 
                    required
                    value={verifyPassword}
                    onChange={e => setVerifyPassword(e.target.value)}
                />
            </Form.Group>

            { isActive ?
                <Button className="register-button" variant="outline-primary" type="submit" id="submitBtn">CREATE MY ACCOUNT</Button>
                :
                <Button  className="register-button" variant="outline-primary" type="submit" id="submitBtn" disabled>CREATE MY ACCOUNT</Button>
            }
        </Form>
        </>
    );
}