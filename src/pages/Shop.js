import { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import ShopList from '../components/ShopList';
import UserContext from '../UserContext';
import '../style/styles.css';

export default function Shop() {
    const { user } = useContext(UserContext);
    const redirect = useNavigate();
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch("https://shrouded-coast-15740.herokuapp.com/products/active")
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setProducts(data.map(product => {
                return (
                    <ShopList key={product._id} productProp={product}/>
                )
            }))
        })
    }, []);
    
    // console.log(user.isAdmin);
    return (
        (user.isAdmin === true) ?
            redirect('/')
        :
        <>
            <h1 className="title-header">AVAILABLE PRODUCTS</h1>
            <div className="grid">
                {products}
            </div>
        </>
    )
}