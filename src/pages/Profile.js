import { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import AdminProfile from '../components/AdminProfile';
import UserProfile from '../components/UserProfile';
import UserContext from '../UserContext';

export default function Profile() {
    const { user } = useContext(UserContext);

    return (
        ((user.id === null) ?
            <Navigate to="/" />
        :
            (user.isAdmin) ? 
                <AdminProfile /> 
            : 
                <UserProfile />
        )
    );
}