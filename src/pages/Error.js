import { Image} from 'react-bootstrap';

export default function Error() {
    return (
        <div className='error-container'>
            <Image className="error-logo" src={require('../images/error-404-logo.png')} />
            <h1>404</h1>
            <h4>Page not found</h4>
            <p>The page you are looking for doesn't exist or an other error occured.<br />
            Go back or head over to our HOME page and choose new direction.</p>
        </div>
    );
}